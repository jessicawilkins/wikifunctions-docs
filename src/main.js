import { createApp } from "vue";
import { createI18n } from "vue-i18n";
import App from "./App.vue";
import en from "./i18n/en.json";
import ar from "./i18n/ar.json";

const i18n = createI18n({
  locale: "en",
  messages: {
    en: {
      dir: "ltr",
      message: en,
    },
    ar: {
      dir: "rtl",
      message: ar,
    },
  },
});

createApp(App).use(i18n).mount("#app");
